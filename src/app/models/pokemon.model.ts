

export interface PokemonDetail  {
    id: number;
    order: number;
    name: string;
    height: number;
    abilities: Ability[];
    spices: Species;
    types: Type[];
    weight: number;
    sprites: Sprites;
    stats: Stat[];

}
 

        export interface Ability2 {
            name: string;
        }
    
        export interface Ability {
            ability: Ability2;
        }

     
    
   
    
        export interface Species {
            name: string;
        }
    

    
        export interface Sprites {
            front_default: string;
        }
    
        export interface Stat2 {
            name: string;
          
        }
    
        export interface Stat {
            base_stat: number;
            stat: Stat2;
        }
    
        export interface Type2 {
            name: string;
        }
    
        export interface Type {
            slot: number;
            type: Type2;
        }
    
     
    
    
   
  

