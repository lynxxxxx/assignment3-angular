import { PokemonList } from "./pokemonList";

export interface Trainer {
    id: number;
    username: string;
    pokemon: PokemonList[];
}
