export enum StorageKeys {
    Trainer = "pokemon-trainer",
    Pokemons = "pokemons"
}