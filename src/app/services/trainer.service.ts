// this is keeping track of the currently logged in users

import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs';
import { environment } from 'src/environments/environment';
import { StorageKeys } from '../enums/storage-keys.enum';
import { PokemonDetail } from '../models/pokemon.model';
import { PokemonList, } from '../models/pokemonList';
import { Trainer } from '../models/trainer.model';
import { StorageUtil } from '../utils/storage.util';

const {apiPokemon,apiTrainers, apiImgUrl} = environment;


@Injectable({
  providedIn: 'root',
})
export class TrainerService {
  private _trainer?: Trainer; //user can be undefined or of type user
  private _error: string = "";
  private _loading: boolean = false;

  private _pokemon: PokemonDetail[] = [];

  get trainer(): Trainer | undefined {
    return this._trainer;
  }

  get pokemon(): PokemonDetail[] {
    return this._pokemon
   }

 
 
   get error(): string {
     return this._error;
   }
 
   get loading(): boolean {
     return this._loading;
   }
 

  set trainer(trainer: Trainer | undefined) {
    StorageUtil.storageSave<Trainer>(StorageKeys.Trainer, trainer!); //The "user!" will never be undefined. basically they will be a user
    this._trainer = trainer;
  }
  constructor( private http: HttpClient) {
    this._trainer = StorageUtil.storageRead<Trainer>(StorageKeys.Trainer);
  }

  // checking if favourites if true or false
  public inFavourites(pokemonId: number| any): boolean {
    if (this._trainer) {
       return Boolean(this.trainer?.pokemon.find((pokemon: PokemonDetail | any) => pokemon.id === pokemonId))    
    }
           
      return false 
     }


// this is adding pokemon to favourites 
public addToFavourites(pokemon: PokemonDetail | any): void {
  if (this._trainer) {
    this._trainer.pokemon.push(pokemon)
    
  }
 }



//removing favourites from user 

 public removeFromFavourites(pokemonId: number): void {
  if(this._trainer) {
    this._trainer.pokemon = this._trainer.pokemon.filter((poke: PokemonDetail | any) => poke.id !== pokemonId)

  }
 }
  

}