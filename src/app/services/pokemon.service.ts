import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { forkJoin, map, Observable, pipe } from 'rxjs';
import { environment } from 'src/environments/environment';
import { StorageKeys } from '../enums/storage-keys.enum';
import { PokemonDetail } from '../models/pokemon.model';
import { PokemonList } from '../models/pokemonList';
import { StorageUtil } from '../utils/storage.util';

const {apiPokemon, apiImgUrl} = environment;

@Injectable({
  providedIn: 'root'
})
export class PokemonService {
 
 

  private _error: string = "";
  private _loading: boolean = false;
  private _pokemon: PokemonDetail[] = [];


  

 

   get pokemon(): PokemonDetail[] | any {
    return this._pokemon
   }
 
   get error(): string {
     return this._error;
   }
 
   get loading(): boolean {
     return this._loading;
   }

   set pokemon(poke: PokemonDetail  | any) {
    StorageUtil.storageSave<PokemonDetail>(StorageKeys.Pokemons, poke!); //The "user!" will never be undefined. basically they will be a user
    this._pokemon = poke;
  }
  
  constructor(
    private readonly http: HttpClient
   
  ) {}


//Pokemon details set to an empty array or undefined. Reading the pokemon from local storage. If pokemon is not undefined then set pokemon else set pokemon 
  getPage() {

    this.getPokemonList()
    .subscribe((list: PokemonList[]) => {
      if(list.length === 0) {
        throw new Error("There is no data in PokemonList")
        
      } else {
        this.getPokemon(list);
      }
    });
    
}
    // we are getting the list (pokemon). Registering the pokemon details and setting it into the array. 
private getPokemon(list: PokemonList[]) {
  const arr: Observable<PokemonDetail>[] = [];
  list.map((value: PokemonList) => {
    arr.push(
      this.getPokemonDetail(value.name)
    );
    
    
  });

  forkJoin([...arr]).subscribe((pokemons: PokemonDetail[]) => {
    this._pokemon = pokemons;
   // this._pokemon.push(...pokemons);
    StorageUtil.storageSave(StorageKeys.Pokemons, this._pokemon);
   
  })  
  }

  
   

     
  //getting pokemon id
  public pokemonById(id: number | any ): PokemonDetail | undefined {
    return this._pokemon.find((pokemon: PokemonDetail) => pokemon.id === id);
  
    
  }
//getting fetch
  getPokemonList(limit: number = 26) : Observable<PokemonList[]> {
    return this.http.get<PokemonList[]>(`${apiPokemon}?limit=${limit}`)
    .pipe(
      map((x: any) => x.results)
  );
}
//getting details
  getPokemonDetail(pokemon: number | string): Observable<PokemonDetail> {
    return this.http.get<PokemonDetail>(`${apiPokemon}/${pokemon}`);
}

}


