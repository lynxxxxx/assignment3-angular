import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map, Observable, switchMap, of } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Trainer } from '../models/trainer.model';

const { apiTrainers, apiKey } = environment;

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(private readonly http: HttpClient) {}

  // Models, Observables, and RxJS operators Login
  public login(username: string): Observable<Trainer> {
    //User interface
    return this.checkUsername(username)
    .pipe(
      switchMap((trainer: Trainer | undefined) => {
        //så att man kan switcha fron checkuser till createuser
        if (trainer === undefined) {
          //user dose not exist
          return this.createUser(username);
        }
        return of(trainer);
      })
      
    );
  }

  //Check if user exist
  private checkUsername(username: string): Observable<Trainer | undefined> {
    return this.http.get<Trainer[]>(`${apiTrainers}?username=${username}`).pipe(
      //rxJS Operators
      map((trainer: Trainer[]) => trainer.pop())
    );
  }

  //If not user - Create a User
  private createUser(username: string): Observable<Trainer> {
    // new user if it no exist
    const trainer = {
      username,
      pokemon: [],
    };
    //header -> API Key
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'x-api-key': apiKey
    });
    //POST - Create items on the server
    return this.http.post<Trainer>(apiTrainers, trainer, {
      headers,
    });
  }


}
