import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Observable, tap } from 'rxjs';
import { Trainer } from '../models/trainer.model';
import { PokemonService } from './pokemon.service';
import { TrainerService } from './trainer.service';
import { PokemonDetail } from '../models/pokemon.model';


const {apiTrainers, apiKey} = environment;

@Injectable({
  providedIn: 'root'
})
export class FavouriteService {

  // private _loading: boolean = false;

  // get loading(): boolean {
  //   return this._loading
  // }

  constructor(
    private http: HttpClient,
    private readonly pokemonService: PokemonService,
    private readonly trainerService: TrainerService,
  ) { }

  //get the guitar based on the Id

  //patch request with the userId and the guitar

 public addToFavourites(pokemonId: number): Observable<Trainer> {
  //checking if user exist
  if (!this.trainerService.trainer) {
    throw new Error("addToFavourites: There is no user");
  }
  //we have a user
  const trainer: Trainer = this.trainerService.trainer;

  //Getting the current Pokemon 
  const pokemon: PokemonDetail | undefined = this.pokemonService.pokemonById(pokemonId)

  //if pokemon doesn't exist then throw an error with the addToFavourites saying no ID
  if (!pokemon) {
    throw new Error("addToFavourites: No pokemon with id: " + pokemonId)
  }
   
  // if favourite is true then we remove from favourites else we add it to the favourites 
  if (this.trainerService.inFavourites(pokemonId)) {
    console.log(pokemonId);
    
    this.trainerService.removeFromFavourites(pokemonId);
    
  } else {
     this.trainerService.addToFavourites(pokemon);
  }
//authentication 
  const headers = new HttpHeaders({
    'Content-Type': 'application/json',
    'x-api-key': apiKey
  });


//updating the partial information of the trainer 
  return this.http.patch<Trainer>(`${apiTrainers}/${trainer.id}`, {
    
   
    pokemon: [...trainer.pokemon]//user favourite already updated
   
  }, {
    headers
  })
  //filtering the trainer and then updating. 
  .pipe(
    tap((updateTrainer: Trainer) => {
      this.trainerService.trainer = updateTrainer
    })
   
  )
 }
}
