import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { StorageKeys } from 'src/app/enums/storage-keys.enum';
import { TrainerService } from 'src/app/services/trainer.service';
import { StorageUtil } from 'src/app/utils/storage.util';

@Component({
  selector: 'app-logout-button',
  templateUrl: './logout-button.component.html',
  styleUrls: ['./logout-button.component.css']
})
export class LogoutButtonComponent implements OnInit {

  constructor(
    private readonly trainerService: TrainerService,
    private readonly router: Router
  ) { }

  ngOnInit(): void {
  }

  handleLogout(): void {
    if (this.trainerService.trainer) {
      StorageUtil.storageRemove(StorageKeys.Trainer);
      this.router.navigateByUrl("/login");
      this.trainerService.trainer = undefined;
    }
  }

}
