import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Trainer } from 'src/app/models/trainer.model';
import { LoginService } from 'src/app/services/login.service';
import { TrainerService } from 'src/app/services/trainer.service';

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.css']
})
export class LoginFormComponent implements OnInit {

  @Output() login: EventEmitter<void> = new EventEmitter();

  constructor(
    private readonly loginService: LoginService,
    private readonly trainerService: TrainerService
  ) { }



  public loginSubmit(loginForm: NgForm): void {
    //void because is not return anything
    //username
    const { username } = loginForm.value;
    // console.log(username);
    // return;
    if(username !== ''){
      this.loginService.login(username).subscribe({
        //reagerar värje gång man clicker på knappen
        next: (trainer: Trainer) => {//once it receaves a data it will com to next
          //redirect to catalog page
          this.trainerService.trainer = trainer
          this.login.emit();
        },
        error: () => {},
      });
    } else {
      alert('you need to write your name')
    }

   
  }

  ngOnInit(): void {
  
  }

}
