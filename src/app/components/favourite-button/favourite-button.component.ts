import { HttpErrorResponse } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { Trainer } from 'src/app/models/trainer.model';
import { FavouriteService } from 'src/app/services/favourite.service';
import { TrainerService } from 'src/app/services/trainer.service';


@Component({
  selector: 'app-favourite-button',
  templateUrl: './favourite-button.component.html',
  styleUrls: ['./favourite-button.component.css']
})
export class FavouriteButtonComponent implements OnInit {

  public loading: boolean = false

  public isFavourite: boolean = false;

  @Input() pokemonId: number = 0;// input avalible once view has inesialized

  // get loading(): boolean {
  //   return this.favouriteService.loading
  // }

  // get isFavourite(): boolean {// get initieras så fort som class skapas
  //   return this.userService.inFavourites(this.pokemonId)
  // }

  constructor(
    private trainerService: TrainerService,
    private readonly favouriteService: FavouriteService
  ) { }

  //this only runs once
  ngOnInit(): void {
    //input are resolved
    this.isFavourite = this.trainerService.inFavourites(this.pokemonId)
    console.log(this.isFavourite);
    
  }

  onFavouriteClick(): void {
    this.loading = true;
    //add the pokemon ti the favourites
    this.favouriteService.addToFavourites(this.pokemonId)
    .subscribe({
      next: (response: Trainer) => {
         console.log("NEXT", response);
         //för att sidan  ska updateras automatirle
         this.loading = false
         this.isFavourite = this.trainerService.inFavourites(this.pokemonId)
         
      },
      error: (error: HttpErrorResponse) => {
        console.log("Error", error.message);
        
      }
    })
  }

}
