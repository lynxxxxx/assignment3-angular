import { Component, Input, OnInit } from '@angular/core';
import { PokemonDetail } from 'src/app/models/pokemon.model';
import { Trainer } from 'src/app/models/trainer.model';
//import { PokemonList } from 'src/app/models/pokemonList';
import {MatDialog} from '@angular/material/dialog';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-pokemon-list-item',
  templateUrl: './pokemon-list-item.component.html',
  styleUrls: ['./pokemon-list-item.component.css']
})
export class PokemonListItemComponent implements OnInit {



  @Input() pokemon?: PokemonDetail
  
  constructor() { }

  ngOnInit(): void {
  }

  //Modular for detailed information 
  alertConfirmation() {
    Swal.fire({
      title: "weight: " + this.pokemon?.weight + "<br/>" + "height: " + this.pokemon?.height,
      icon: 'warning',
      
    })
  }
}
