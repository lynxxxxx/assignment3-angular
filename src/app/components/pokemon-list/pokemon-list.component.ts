import { Component, Input, OnInit } from '@angular/core';
import { PokemonDetail } from 'src/app/models/pokemon.model';

@Component({
  selector: 'app-pokemon-list',
  templateUrl: './pokemon-list.component.html',
  styleUrls: ['./pokemon-list.component.css']
})
export class PokemonListComponent implements OnInit {

 
// pagination 
 
  searchPokemon = '';
  public directionLinks: boolean = true;
  public responsive: boolean = true;
  
   public config:  any;

  @Input() pokemons: PokemonDetail[] = [];

  constructor() {
    this.config = {
      itemsPerPage: 20,
      currentPage: 1,
      totalItems: this.pokemons?.length
    };
    
   }

   

  ngOnInit(): void {
  }

   pageChanged(event: any){
    this.config.currentPage = event;
  }
  
// search filter 
  search(value: string): void {
    this.pokemons.filter((val: any) =>
      val.name.toLowerCase().includes(value)
    );
  }

 

}
