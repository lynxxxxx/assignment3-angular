import { Component, Input, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs';
import { PokemonDetail } from 'src/app/models/pokemon.model';
import { MatTableDataSource } from '@angular/material/table';

import { PokemonService } from 'src/app/services/pokemon.service';

@Component({
  selector: 'app-pokemon-catalogue',
  templateUrl: './pokemon-catalogue.page.html',
  styleUrls: ['./pokemon-catalogue.page.css']
})
export class PokemonCataloguePage implements OnInit {




  get pokemons(): PokemonDetail[] | any {
    return this.pokemonService.pokemon;
  }


  get error(): string {
    return this.pokemonService.error;
  }
  
  get loading(): boolean {
    return this.pokemonService.loading;
  }

  constructor(
 
    private readonly pokemonService: PokemonService
  ) { }

  ngOnInit(): void {
    
    this.pokemonService.getPage()
  //  console.log(this.pokemonService.dd);
    
  }
  

}
