import { Component, OnInit } from '@angular/core';
import { PokemonDetail } from 'src/app/models/pokemon.model';
import { Trainer } from 'src/app/models/trainer.model';
import { TrainerService } from 'src/app/services/trainer.service';

@Component({
  selector: 'app-trainer',
  templateUrl: './trainer.page.html',
  styleUrls: ['./trainer.page.css']
})
export class TrainerPage implements OnInit {
//get trainer method which can also be undefined 
  get trainer(): Trainer | undefined {
    return this.trainerService.trainer
  }
  //get favourites method and setting the favourites into the pokemon detail. if there is a user then return pokemon which is already inside the collection

  get favourites(): PokemonDetail[] | any {
    if (this.trainerService.trainer) {
      console.log(this.trainerService.trainer);
      return this.trainerService.trainer.pokemon.filter(this.checkIfArrayNoEmpty)
    }
    return []
  }

  constructor(
    private trainerService: TrainerService,
  ) { }

  ngOnInit(): void {
  }

  //basically checking if the array is empty. 

  public checkIfArrayNoEmpty(item: any) {
      if(item === "bulbasaur") {
       return item !== "bulbasaur" 
      }
      else if ( item === "pikachu") {
        return item !== "pikachu"
      }
      return item
  }

}
