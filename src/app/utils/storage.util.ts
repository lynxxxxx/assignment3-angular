export class StorageUtil {
// saving local storage 
    public static storageSave<T>(key: string, value: T): void {
        sessionStorage.setItem(key, JSON.stringify(value));
    }
//reading local storage 
    public static storageRead<T>(key: string): T | undefined {
        const storedValue = sessionStorage.getItem(key);

        try {
            if (storedValue) {
                return JSON.parse(storedValue) as T;
            } 
            return undefined;
            
        } catch(e) {
        //if invalid json if it fail during process remove key from storage
            sessionStorage.removeItem(key)
            return undefined
        }
    }
// removing from local storage
    public static storageRemove(key: string): void {
        localStorage.removeItem(key)
    }
}