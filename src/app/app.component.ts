import { Component, OnInit } from '@angular/core';
import { PokemonService } from './services/pokemon.service';
import { TrainerService } from './services/trainer.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  
  constructor(
    private readonly trainerService: TrainerService,
    private readonly pokemonService: PokemonService,
   ){ }
 
   ngOnInit(): void {
    // fetching lister 
     if(this.trainerService.trainer) {
       this.pokemonService.getPage()
     }
   }
}
