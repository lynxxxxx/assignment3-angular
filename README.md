# Assignment3Angular

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 14.0.6.

### tailwind
npm install -D tailwindcss
npx tailwindcss init

### google material icons in index.html
 <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
      rel="stylesheet">

### How to use website



User types in username in name field and continues by clicking on "Login" button.



Available pokemons will be displayed on the "Pokemon Catalogue Page", user "catches" each pokemon by clicking on "star" icon on corner of a pokemon.

User can search names of pokemon. User can also get detailed information on various pokemon.



The catched pokemon will be shown on the "profile" page. Which will be the favourites 

### material
ng add @angular/material

### search filter
npm i ng2-search-filter

### Pagination
npm install ngx-pagination

### Modal
npm i sweetalert2

### 1 Login
check if the user already exists in DB, if the user does not exist, a new user is created and saved in DB

### 2 Pokemon 
The profile lists 100 pokemons

### 3 Profile
The profile shows your catched pokemons

## COntributors 
[Daniel Sholaja (DanSho)] 
[Rinat Iunusov (@lynxxxxx)]
